package org.example.playwrite.pages;

import com.microsoft.playwright.Page;

public abstract class MainPage<T extends MainPage> extends BasePage{
    private final String gearItem = "#ui-id-6";
    private final String bagsItem = "#ui-id-25";
    private final String cartIcon = "a.showcart";
    private final String cartIconItemsNumber = "a.showcart span.counter-number";
    private final String proceedToCheckoutBtn = "#top-cart-btn-checkout";

    public MainPage(Page page) {
        super(page);
    }

    public CategoryPage openBagsCategory() {
        page.hover(gearItem);
        page.click(bagsItem);
        return new CategoryPage(page);
    }
    public T checkCartItemsCount(String expectedCount) {
        page.waitForCondition(() -> page.textContent(cartIconItemsNumber).equals(expectedCount));
        return (T) this;
    }

    public CheckoutPage proceedToCheckout() {
        page.click(cartIcon);
        page.click(proceedToCheckoutBtn);
        return new CheckoutPage(page);
    }

}
