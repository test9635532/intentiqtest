package org.example.playwrite.pages;

import com.microsoft.playwright.Page;

public class SuccessPage extends BasePage {
    private String orderNumber = ".checkout-success > p > span";

    public SuccessPage(Page page) {
        super(page);
    }

    public String getOrderNumber() {
        return page.textContent(orderNumber);
    }
}
