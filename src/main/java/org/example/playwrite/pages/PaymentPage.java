package org.example.playwrite.pages;

import com.microsoft.playwright.Page;

public class PaymentPage extends BasePage {
    private String pageTitle = ".payment-methods .step-title";
    private String placeOrderButton = "button[title='Place Order']";
    public PaymentPage(Page page) {
        super(page);
    }

    public SuccessPage clickPlaceOrder() {
        page.click(placeOrderButton);
        return new SuccessPage(page);
    }
}
