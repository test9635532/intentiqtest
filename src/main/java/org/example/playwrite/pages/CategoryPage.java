package org.example.playwrite.pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.WaitForSelectorState;

public class CategoryPage extends MainPage<CategoryPage> {
    private final String productItem = "li.product-item:has(img[alt='${itemName}'])";
    private final String addToCartBtn = "button[title='Add to Cart']";
    private final String alert = "div.messages[role='alert']";

    public CategoryPage(Page page) {
        super(page);
    }

    public CategoryPage addItemToCart(String itemName) {
        Locator item = page.locator(productItem.replace("${itemName}", itemName));
        item.hover();
        item.locator(addToCartBtn).click();
        page.waitForSelector(alert, new Page.WaitForSelectorOptions().setState(WaitForSelectorState.VISIBLE));
        return this;
    }
}
