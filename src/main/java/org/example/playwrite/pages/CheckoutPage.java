package org.example.playwrite.pages;

import com.microsoft.playwright.Page;
import org.example.playwrite.dto.AddressDTO;

import java.util.List;
import java.util.regex.Pattern;

public class CheckoutPage extends BasePage {

    private final String emailField = "#customer-email-fieldset input[name='username']";
    private final String firstNameField = "input[name='firstname']";
    private final String lastNameField = "input[name='lastname']";
    private final String companyField = "input[name='company']";
    private final String addressLineField = "input[name='street[${index}]']";
    private final String cityField = "input[name='city']";
    private final String stateField = "input[name='region']";
    private final String postalCodeField = "input[name='postcode']";
    private final String countryField = "select[name='country_id']";
    private final String phoneField = "input[name='telephone']";
    private final String nextBtn = "button.action.continue";


    public CheckoutPage(Page page) {
        super(page);
    }

    public CheckoutPage fillTheForm(String email, AddressDTO address) {
        fillTheField(emailField, email);
        fillTheField(firstNameField, address.getFirstname());
        fillTheField(lastNameField, address.getLastname());
        fillTheField(companyField, address.getCompany());
        fillStreetLines(address.getStreet());
        fillTheField(cityField, address.getCity());
        fillTheField(stateField, address.getRegion());
        fillTheField(postalCodeField, address.getPostcode());
        page.selectOption(countryField, address.getCountryId().name());
        fillTheField(phoneField, address.getTelephone());
        page.waitForResponse(Pattern.compile(".*/estimate-shipping-methods"), () -> {});
        return this;
    }

    private void fillTheField(String fieldLocator, String value) {
        if (null != value) {
            page.fill(fieldLocator, value);
        }
    }

    private void fillStreetLines(List<String> values) {
        for (int i = 0; i < Math.min(values.size(), 3); i++) {
            fillTheField(addressLineField.replace("${index}", String.valueOf(i)), values.get(i));
        }
    }

    public PaymentPage clickNextBtn() {
        page.click(nextBtn);
        return new PaymentPage(page);
    }
}
