package org.example.playwrite;

import com.microsoft.playwright.*;
import org.example.playwrite.exception.NotSupportedBrowserException;

import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PlayWriteFactory {
    private Page page;

    public Page getPage(Properties properties) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Browser browser = getBrowserType(properties.getProperty("browser"))
                .launch(getLaunchOptions(properties));
        BrowserContext browserContext = browser.newContext(new Browser.NewContextOptions().setViewportSize(screenSize.width, screenSize.height));
        page = browserContext.newPage();
        page.navigate(properties.getProperty("baseUrl"));
        return page;
    }

    public void close() {
        page.context().browser().close();
    }

    private BrowserType getBrowserType(String browserName) {
        Playwright playwright = Playwright.create();
        BrowserType browserType;
        switch (browserName.toLowerCase()) {
            case "chrome":
                browserType = playwright.chromium();
                break;
            case "firefox":
                browserType = playwright.firefox();
                break;
            case "safari":
                browserType = playwright.webkit();
                break;
            default:
                throw new NotSupportedBrowserException("Browser with name " + browserName + " is not supported");
        }

        return browserType;
    }

    private BrowserType.LaunchOptions getLaunchOptions(Properties properties) {
        BrowserType.LaunchOptions options = new BrowserType.LaunchOptions();
        options.setHeadless(Boolean.getBoolean(properties.getProperty("headless")));
        return options;
    }
}
