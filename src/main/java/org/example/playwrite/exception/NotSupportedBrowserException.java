package org.example.playwrite.exception;


public class NotSupportedBrowserException extends RuntimeException{
    public NotSupportedBrowserException(String message) {
        super(message);
    }
}
