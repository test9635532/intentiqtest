package org.example.playwrite.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonHelper {
    public static <T> T getJsonAs(String json, Class<T> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(json, clazz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
