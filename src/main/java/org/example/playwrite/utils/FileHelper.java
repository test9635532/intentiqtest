package org.example.playwrite.utils;

import java.io.FileWriter;
import java.io.IOException;

public class FileHelper {
    public static void saveToFile(String content, String fileName) {
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
