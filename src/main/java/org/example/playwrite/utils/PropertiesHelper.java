package org.example.playwrite.utils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

public class PropertiesHelper {
    public static Properties getProperties() {
        String propertiesPath = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("playwrite.properties")).getPath();

        Properties properties = new Properties();
        try {
            properties.load(Files.newInputStream(Paths.get(propertiesPath)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }
}
