package org.example.playwrite.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RootDTO {
    private AddressInformationDTO addressInformation;
}
