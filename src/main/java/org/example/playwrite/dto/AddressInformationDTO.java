package org.example.playwrite.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class AddressInformationDTO {
    @JsonProperty("billing_address")
    private AddressDTO billingAddress;
    @JsonProperty("extension_attributes")
    private Object extensionAttributes;
    @JsonProperty("shipping_address")
    private AddressDTO shippingAddress;
    @JsonProperty("shipping_carrier_code")
    private String shippingCarrierCode;
    @JsonProperty("shipping_method_code")
    private String shippingMethodCode;
}
