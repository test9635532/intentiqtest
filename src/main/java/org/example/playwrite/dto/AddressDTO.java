package org.example.playwrite.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressDTO {
    private String firstname;
    private String lastname;
    private String company;
    private List<String> street = new ArrayList<>();
    private String city;
    private String region;
    @JsonProperty("region_id")
    private String regionId;
    private String postcode;
    private Country countryId;
    private String telephone;
    private Boolean saveInAddressBook;

}
