package org.example.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ChromeDriverInitTest {
    WebDriver driver;
    @BeforeTest
    public void initiateChromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("profile.cookie_controls_mode", 1);
        options.setExperimentalOption("prefs", prefs);

        driver = new ChromeDriver(options);
    }

    @Test(testName = "Create a chromedriver instance with required configs")
    public void test() {
        driver.get("chrome://settings/cookies");
        try {
            Thread.sleep(5000);
        } catch (Exception ignored) {}
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
