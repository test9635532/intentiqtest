package org.example.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FireFoxDriverInitTest {
    WebDriver driver;
    private final String userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 16_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.3 Mobile/15E148 Safari/604.1";
    @BeforeTest
    public void initiateChromeDriver() {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();

        options.addPreference("general.useragent.override", userAgent);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("network.cookie.cookieBehavior", 1);

        options.setProfile(profile);

        driver = new FirefoxDriver(options);
    }

    @Test(testName = "Create a firefox instance with required configs")
    public void test() {
        driver.get("https://www.whatismybrowser.com/detect/what-is-my-user-agent/");
        String currentUserAgent = driver.findElement(By.cssSelector("#detected_value > a")).getText();
        Assert.assertEquals(currentUserAgent, userAgent);
        driver.get("about:preferences#privacy");
        try {
            Thread.sleep(5000);
        } catch (Exception ignored) {}
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
