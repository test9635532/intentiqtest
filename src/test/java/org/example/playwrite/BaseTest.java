package org.example.playwrite;

import com.microsoft.playwright.Page;
import org.example.playwrite.utils.PropertiesHelper;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.Properties;

public abstract class BaseTest {
    private PlayWriteFactory factory;
    private Properties properties;
    protected Page page;
    @BeforeTest
    public void init() {
        properties = PropertiesHelper.getProperties();
        factory = new PlayWriteFactory();
        page = factory.getPage(properties);
    }

    @AfterTest
    public void tearDown() {
        factory.close();
    }
}
