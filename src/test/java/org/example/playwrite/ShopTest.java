package org.example.playwrite;

import com.microsoft.playwright.Request;
import org.example.playwrite.dto.AddressDTO;
import org.example.playwrite.dto.AddressInformationDTO;
import org.example.playwrite.dto.Country;
import org.example.playwrite.dto.RootDTO;
import org.example.playwrite.pages.CategoryPage;
import org.example.playwrite.pages.CheckoutPage;
import org.example.playwrite.pages.HomePage;
import org.example.playwrite.pages.PaymentPage;
import org.example.playwrite.utils.FileHelper;
import org.example.playwrite.utils.JsonHelper;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;


public class ShopTest extends BaseTest {
    @Test
    public void test() {
        AddressDTO address = AddressDTO.builder()
                .firstname("John")
                .lastname("Doe")
                .company("IntentIq")
                .street(Arrays.asList("line1", "line2", "line3"))
                .city("Tel Aviv")
                .postcode("6687865")
                .countryId(Country.IL)
                .telephone("1234567890")
                .build();


        HomePage homePage = new HomePage(page);
        CategoryPage categoryPage = homePage.openBagsCategory();
        categoryPage.addItemToCart("Push It Messenger Bag");
        categoryPage.checkCartItemsCount("1");
        CheckoutPage checkoutPage = categoryPage.proceedToCheckout();
        checkoutPage.fillTheForm("some@email.com", address);
        Request request = page.waitForRequest(Pattern.compile(".*/shipping-information"), () -> {
            checkoutPage.clickNextBtn();
        });

        AddressInformationDTO addressInformation = JsonHelper.getJsonAs(request.postData(), RootDTO.class).getAddressInformation();
        assertThat(addressInformation.getBillingAddress())
                .as("wrong billing address")
                .isEqualToIgnoringNullFields(address);

        assertThat(addressInformation.getShippingAddress())
                .as("wrong shipping address")
                .isEqualToIgnoringNullFields(address);


        String orderNumber = new PaymentPage(page).clickPlaceOrder().getOrderNumber();
        FileHelper.saveToFile(orderNumber, "test_data.txt");
    }
}
